This is tiny (one function) package that creates a simple event emitter/listener
pair.

```ts
import { atomicEvent } from "atomic-event";
const [listen, emit] = atomicEvent();

const unlisten = listen((ev) => console.log(ev));
emit("hello"); // logs "hello".

unlisten();
emit("hello"); // nothing happens
```

## TypeScript

`atomicEvent` accepts one type argument that signifies the event type. This
defaults to `unknown`.

```ts
atomicEvent<string>();
```

## Disposer

The listener function (above, `listen()`) returns an unlistener function.

Each time you call `listen()`, a disposer that you receive is unique and
idempotent. That is, each disposer unlistens only the one instance of call to
`listen()` that produced it, even if same (read: referentially idential) callback
function was previously registered.
