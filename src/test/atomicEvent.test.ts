import { atomicEvent } from "@lib";

test("atomic event", () => {
  const [listen, emit] = atomicEvent<number>();

  const listener1 = jest.fn((ev: number) => void 0);
  const listener2 = jest.fn((ev: number) => void 0);

  const unlisten1 = listen(listener1);
  const unlisten2 = listen(listener2);

  emit(10);
  expect(listener1).toHaveBeenCalledTimes(1);
  expect(listener1).toHaveBeenLastCalledWith(10);
  expect(listener2).toHaveBeenCalledTimes(1);
  expect(listener2).toHaveBeenLastCalledWith(10);

  emit(20);
  expect(listener1).toHaveBeenCalledTimes(2);
  expect(listener1).toHaveBeenLastCalledWith(20);
  expect(listener2).toHaveBeenCalledTimes(2);
  expect(listener2).toHaveBeenLastCalledWith(20);

  unlisten1();

  emit(30);
  expect(listener1).toHaveBeenCalledTimes(2);
  expect(listener2).toHaveBeenCalledTimes(3);
  expect(listener2).toHaveBeenLastCalledWith(30);

  unlisten2();

  emit(40);
  expect(listener1).toHaveBeenCalledTimes(2);
  expect(listener2).toHaveBeenCalledTimes(3);
});

test("every disposer is unique and idempotent", () => {
  const [listen, emit] = atomicEvent<number>();

  const listener = jest.fn((ev: number) => void 0);
  const unlisten1 = listen(listener);
  const unlisten2 = listen(listener);
  const unlisten3 = listen(listener);

  emit(10);
  expect(listener).toHaveBeenCalledTimes(3);
  expect(listener).toHaveBeenNthCalledWith(1, 10);
  expect(listener).toHaveBeenNthCalledWith(2, 10);
  expect(listener).toHaveBeenNthCalledWith(3, 10);

  unlisten1();

  emit(30);
  expect(listener).toHaveBeenCalledTimes(5);
  expect(listener).toHaveBeenNthCalledWith(4, 30);
  expect(listener).toHaveBeenNthCalledWith(5, 30);

  // idempotent, so noop this time
  unlisten1();

  emit(50);
  expect(listener).toHaveBeenCalledTimes(7);
  expect(listener).toHaveBeenNthCalledWith(6, 50);
  expect(listener).toHaveBeenNthCalledWith(7, 50);

  unlisten2();

  emit(70);
  expect(listener).toHaveBeenCalledTimes(8);
  expect(listener).toHaveBeenNthCalledWith(8, 70);

  unlisten3();
  expect(listener).toHaveBeenCalledTimes(8);
});
