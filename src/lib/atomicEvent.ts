/**
 * Creates a simple event listener/emitter pair.
 */
export function atomicEvent<T>(): AtomicEvent<T> {
  const listeners = new Set<Listener<T>>();
  const emit = (ev: T) => {
    for (const cb of listeners) cb(ev);
  };
  const listen = (cb: Listener<T>): Disposer => {
    const uniqueCb: Listener<T> = (ev) => cb(ev);
    listeners.add(uniqueCb);
    return () => void listeners.delete(uniqueCb);
  };
  return [listen, emit];
}

export type AtomicEvent<T> = readonly [
  listen: (cb: Listener<T>) => Disposer,
  emit: (ev: T) => void
];

export interface Disposer {
  (): void;
}

export type Listener<T> = (ev: T) => void;
